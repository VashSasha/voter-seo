// smooth scroll
$(document).ready(function(){
    $(".navbar .nav-link").on('click', function(event) {

        if (this.hash !== "") {

            event.preventDefault();

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 700, function(){
                window.location.hash = hash;
            });
        } 
    });
});

//Collapse dropdown
(function() {
    const dropdown = $('.navbar-collapse');
    const tgl = $('.navbar-toggler');
    $(document).click(function (e) {
        if(!e.target.classList.contains('navbar-toggler') && dropdown.hasClass('show')) {
            console.log('has show ');
            tgl.click();
        }
    });
}
)();


$("#pop").on("click", function() {
    $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
    $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
});